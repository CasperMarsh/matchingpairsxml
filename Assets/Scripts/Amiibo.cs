﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amiibo {

	private int Identifier;
	private string FullName;
	private Texture2D AmiiboTexture;

	public Amiibo(int identifier,  string spriteName, string fullName) {
		this.Identifier = identifier;
		this.FullName = fullName;
		this.AmiiboTexture = Resources.Load<Texture2D>("images/" + spriteName);
	}

	public int getIdentifier()
	{
		return Identifier;
	}

	public string getFullName()
	{
		return FullName;
	}

	public Texture2D getAmiiboTexture()
	{
		return AmiiboTexture;
	}
	
}

