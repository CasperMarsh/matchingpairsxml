﻿using UnityEngine;
using System.Collections;

public class BoardSquare : MonoBehaviour
{
	protected Texture2D[] images = new Texture2D[3];
	protected GameObject textBox;
	
	private Color color = new Color(1.0f,0.0f,0.0f,0.25f);

	protected virtual void onSquareSelected ()
	{
		
		textBox.GetComponent<Renderer>().enabled = true;

	}

	public virtual void changeSquareStatus(Color color, string str){		
			
	}

	
	protected virtual void updateEndGraphic ()
	{
		
	}
	
	public virtual void resetSquare ()
	{	
		textBox.GetComponent<Renderer>().enabled = false;
		this.GetComponent<Collider>().enabled = true;

	}

	public void showSquare() {
		
	}
	
	protected virtual void Awake ()
	{
		images [0] = Resources.Load ("Square") as Texture2D;
		images [1] = Resources.Load ("Square") as Texture2D;
		images [2] = Resources.Load ("Square") as Texture2D;
	}
	
	protected virtual void Start() {
		this.GetComponent<Renderer>().material.mainTexture = images [1];
		textBox = this.transform.Find("TextBox").gameObject;
		textBox.GetComponent<Renderer>().enabled = false; //turns text box visibility off
	}
	
	protected virtual void OnMouseDown ()
	{
		this.GetComponent<Renderer>().material.mainTexture = images [1];
		this.GetComponent<Collider>().enabled = false;
		onSquareSelected ();
	}
	
}
